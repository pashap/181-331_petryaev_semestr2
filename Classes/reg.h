#ifndef REG_H
#define REG_H

#include <QDialog>
#include "QMessageBox"
#include <QStatusBar>

namespace Ui {
class reg;
}

class reg : public QDialog
{
    Q_OBJECT

public:
    explicit reg(QWidget *parent = nullptr);
    ~reg();

private slots:
    void on_run_clicked();

    // void on_radioButton_2_clicked();

private:
    Ui::reg *ui;
};

#endif // REG_H
