﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "reg.h"
#include "user_window.h"
#include "admin_window.h"
#include "worker.h"
#include "forget_pass.h"

#include "QMessageBox"
#include <QString>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_sign_in_clicked()
{
    QString login = ui->login->text();
    QString password = ui->password->text();

    string Login = login.toUtf8().constData();
    string Password = password.toUtf8().constData();


    QString qs = QString::fromLocal8Bit(Login.c_str());
    QString hellouser = "Hello " + qs;

    ifstream input;

    input.open("C:/Users/pasha/Documents/Classes/users.txt");

    string lg;
    lg = "";
    int k = 0;
    while(!input.eof())
    {
        getline(input, lg);
        if (lg == (Login + ":" + Password + ":admin"))
        {  
            //QMessageBox::information(this, "Yes", "HELLO ADMIN! WELCOME TO HELL!");
            QMessageBox::information(this, "Yes", hellouser);
            admin_window win;
            hide();

            win.setModal(true);
            win.exec();
            hide();

            k = 0;
            lg="";
            break;
        }

        if (lg == (Login + ":" + Password + ":user"))
        {

            QMessageBox::information(this, "Yes", hellouser);
            user_window win(nullptr, login);

            k = 0;
            hide();

            win.setModal(true);
            win.exec();
            hide();

            lg="";
            break;
            //ui->statusBar
        }

//        if (lg == (Login + ":" + Password + ":worker"))
//        {

//            QMessageBox::information(this, "Yes", hellouser);
//            worker win;

//            k = 0;
//            hide();

//            win.setModal(true);
//            win.exec();
//            hide();

//            lg="";
//            break;
//            //ui->statusBar
//        }
         k++;
    }
   if(k > 0)
   {
        QMessageBox::warning(this, "Ошибка!", "Неправильный логин или пароль");
        ui->statusBar->showMessage("Неправильный логин или пароль");
   }

   ui->login->clear();
   ui->password->clear();
   show();

    /*if(login == "admin" && password == "admin")
    {
        QMessageBox::information(this, "Yes", "Hello admin!");
        ui->statusBar->showMessage("Вы успешно авторизовались");
    }else
    {
        QMessageBox::warning(this, "Warning!", "Неправильный логин или пароль");
        ui->statusBar->showMessage("Неправильный логин или пароль");
    }*/

}
void MainWindow::on_register_2_clicked()
{
    hide();
    //window1 = new reg(this);
    //window1->show();
    reg window1;
    window1.setModal(true);
    window1.exec();

    show();

    //QString new_login = ui->new_login->text();
    //QString new_password = ui->new_pass->text();

    //string new_Login = new_login.toUtf8().constData();
    //string new_Password = new_password.toUtf8().constData();
}


void MainWindow::on_forget_clicked()
{
    hide();

    forget_pass window2;
    window2.setModal(true);
    window2.exec();

    show();
}
