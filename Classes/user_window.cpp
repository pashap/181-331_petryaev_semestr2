﻿#include "user_window.h"
#include "ui_user_window.h"
#include "mainwindow.h"
#include "db.h"

user_window::user_window(QWidget *parent, QString login) :
    QDialog(parent),
    ui(new Ui::user_window),
    _login(login)
{
    ui->setupUi(this);
    this->setWindowTitle("Ветеринарная лечебница");

    DB_Test1 user("DB");
    for (int i = 0;i < user._db.size();i++) {
        if (user._db[i].name.c_str() == _login)
        {
            ui->comboBox->addItem(user._db[i].animal.c_str());
        }
    }
}

user_window::~user_window()
{
    delete ui;
}

void user_window::on_exit_to_menu_clicked()
{
    hide();
}

void user_window::on_pushButton_clicked()
{
    ui->Disieases->clear();
    ui->dates->clear();

    DB_Test1 user("DB");

    int k=0;

    for (int i = 0;i < user._db.size();i++) {
        if (ui->comboBox->currentText() == user._db[i].animal.c_str())
        {
            ui->Disieases->setText(user._db[i].disease.c_str());
            ui->dates->setText(user._db[i].date.c_str());
            //user._db[i].name = "";
        }
    }

    //ui->Disieases->setText(user._db[i]..c_str())
}
