#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "reg.h"
#include "user_window.h"
#include "admin_window.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_sign_in_clicked();

    void on_register_2_clicked();

    void on_forget_clicked();

private:
    Ui::MainWindow *ui;
    reg *window1;
};

#endif // MAINWINDOW_H
