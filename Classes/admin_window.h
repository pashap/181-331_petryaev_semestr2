﻿#ifndef ADMIN_WINDOW_H
#define ADMIN_WINDOW_H

#include <QDialog>
#include "string"

namespace Ui {
class admin_window;
}

class admin_window : public QDialog
{
    Q_OBJECT

public:
    explicit admin_window(QWidget *parent = nullptr);
    ~admin_window();

private slots:
    void on_new_add_clicked();

    void on_exit_to_menu_clicked();

    void on_check_users_clicked();
    
    void on_pushButton_clicked();

    void on_release_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::admin_window *ui;
};

#endif // ADMIN_WINDOW_H
