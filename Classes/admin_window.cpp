﻿#include "admin_window.h"
#include "ui_admin_window.h"
#include "db.h"

#include "QMessageBox"
#include "QStandardItemModel"
#include "QStandardItem"
#include <QTableWidget>
#include "QTableView"
#include <QDebug>
#include <QListWidget>
#include <QString>

#include <string>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

admin_window::admin_window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::admin_window)
{
    ui->setupUi(this);
    DB_Test1 nf("DB");
}

admin_window::~admin_window()
{
    delete ui;
}


void admin_window::on_new_add_clicked()
{
    int i = 0;
    QString new_fio = ui->new_fio->toPlainText();
    QString new_animal = ui->new_animal->toPlainText();;
    QString new_disease = ui->new_disease->toPlainText();
    QString new_date = ui->new_date->toPlainText();

    if ((new_fio == "") || (new_animal == "") || (new_disease == "") || (new_date == ""))
    {
         QMessageBox::warning(this, "Error", "������");
         i++;
    }

    if(i==0)
    {
        string New_fio = new_fio.toUtf8().constData();
        string New_animal = new_animal.toUtf8().constData();
        string New_disease = new_disease.toUtf8().constData();
        string New_date = new_date.toUtf8().constData();

        ofstream output;

        DB_Test1 news("DB");

        news.newStr(New_fio, New_animal,New_disease,New_date);

            qDebug() << "NEW" << news._db[0].name.c_str()<<" "<< news._db[0].animal.c_str() <<" "<< news._db[0].disease.c_str() <<" "<< news._db[0].date.c_str() <<" " ;
            qDebug() << "NEW" << news._db[1].name.c_str()<<" "<< news._db[1].animal.c_str() <<" "<< news._db[1].disease.c_str() <<" "<< news._db[1].date.c_str() <<" " ;

        news.fileToVector();
        //output.open("C:\\Users\\pasha\\Documents\\Classes\\DB.txt", fstream::app, fstream::binary);

        //output << New_fio << ":" << New_animal << ":" << New_disease << ":" << New_date << "\n";

        //output.close();

        ui->new_fio->clear();
        ui->new_animal->clear();
        ui->new_disease->clear();
        ui->new_date->clear();

        QMessageBox::information(this, "", "������ ������� ���������");
    }
    //ui->statusBar->showMessage("�����������㧲���ק����������� ���������ק� ���ק����� �����㧳������");

}

void admin_window::on_exit_to_menu_clicked()
{
    hide();
}

void admin_window::on_check_users_clicked()
{

   DB_Test1 in("DB");

    //in._sizeVector();

    //setlocale(LC_ALL,"Russian");

    ui->tableWidget->setRowCount(in._sizeVector()); // ������ ���������� �����
    ui->tableWidget->setColumnCount(4); // ������ ���������� ��������

    QString name = "Name";
    QString ani = "Animal";
    QString dis = "Disease";
    QString date = "Date";

    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << name << ani << dis << date);

       for(int i = 0; i < ui->tableWidget->rowCount(); i++)
           for(int j = 0; j < ui->tableWidget->columnCount();)
           {
               QTableWidgetItem *itm = new QTableWidgetItem(in._db[i].name.c_str());
               ui->tableWidget->setItem(i, j, itm);
               j++;
               QTableWidgetItem *itm1 = new QTableWidgetItem(in._db[i].animal.c_str());
               ui->tableWidget->setItem(i, j, itm1);
               j++;
               QTableWidgetItem *itm2 = new QTableWidgetItem(in._db[i].disease.c_str());
               ui->tableWidget->setItem(i, j, itm2);
               j++;
               QTableWidgetItem *itm3 = new QTableWidgetItem(in._db[i].date.c_str());
               ui->tableWidget->setItem(i, j, itm3);
               j++;
            }

    //cout << nf._db[1].animal;
    //
    //if(nf._db[0].animal == ""){
    //    qDebug() << "fffffffffffffffff";
    //};
    qDebug() << "IN" << in._db[0].name.c_str()<<" "<< in._db[0].animal.c_str() <<" "<< in._db[0].disease.c_str() <<" "<< in._db[0].date.c_str() <<" " ;
    qDebug() << "IN" << in._db[1].name.c_str()<<" "<< in._db[1].animal.c_str() <<" "<< in._db[1].disease.c_str() <<" "<< in._db[1].date.c_str() <<" " ;




   // ui->tableWidget t= new QTableWidget(this);
    //ui->tableWidget->setRowCount(nf._sizeVector);
    //ui->tableWidget->setColumnCount(4);
   // for (int i = 0; i < nf._sizeVector;i++) {
     //   for (int j = 0;j < 4;j++) {
    //        QTableWidgetItem *itm = new QTableWidgetItem;
    //        itm-
    //        if(j == 0)
    //        {ui->tableWidget->setItem(i,j,nf.itm)}
    //    }
   // }
}
void admin_window::on_pushButton_clicked()
{
    ui->tableWidget->clear();

}

void admin_window::on_release_clicked()
{
    DB_Test1 out;

    for(int i = 0; i < ui->tableWidget->rowCount(); i++)
        for(int j = 0; j < ui->tableWidget->columnCount();)
        {
            string name1 = ui->tableWidget->item(i,j)->text().toUtf8().constData();
            j++;
            string animal1 = ui->tableWidget->item(i,j)->text().toUtf8().constData();
            j++;
            string disease1 = ui->tableWidget->item(i,j)->text().toUtf8().constData();
            j++;
            string date1 = ui->tableWidget->item(i,j)->text().toUtf8().constData();
            j++;
            //if((name1 != "") && (animal1 != "") && (disease1 != "") && (date1 == ""))
            //{
                out.newStr(name1,animal1,disease1,date1);
            //}
         }
    //out.vectorToFile();
    qDebug() << "OUT" << out._db[0].name.c_str()<<" "<< out._db[0].animal.c_str() <<" "<< out._db[0].disease.c_str() <<" "<< out._db[0].date.c_str() <<" " ;
    qDebug() << "OUT" << out._db[1].name.c_str()<<" "<< out._db[1].animal.c_str() <<" "<< out._db[1].disease.c_str() <<" "<< out._db[1].date.c_str() <<" " ;


    //QMessageBox::information(this, "", "������ ������� ���������");
    out.vectorToFile();
}

void admin_window::on_pushButton_2_clicked()
{
    DB_Test1 serch("DB");

    ui->tableWidget->setRowCount(serch._sizeVector());
    ui->tableWidget->setColumnCount(4);

    QString name = "Name";
    QString ani = "Animal";
    QString dis = "Disease";
    QString date = "Date";

    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << name << ani << dis << date);

//    qDebug() << "OUT" << serch._db[0].name.c_str()<<" "<< serch._db[0].animal.c_str() <<" "<< serch._db[0].disease.c_str() <<" "<< serch._db[0].date.c_str() <<" " ;
//    qDebug() << "OUT" << serch._db[1].name.c_str()<<" "<< serch._db[1].animal.c_str() <<" "<< serch._db[1].disease.c_str() <<" "<< serch._db[1].date.c_str() <<" " ;


    string serc = ui->search->text().toLocal8Bit().constData();

    int k = 0;

    for (int i = 0;i < serch._db.size(); i++) {
        if ((serc == serch._db[i].name) || (serc == serch._db[i].animal) || (serc == serch._db[i].disease) || (serc == serch._db[i].date))
        {

            //for(int i = 0; i < ui->tableWidget->rowCount(); i++)
                   for(int j = 0; j < ui->tableWidget->columnCount();)
                   {
                       QTableWidgetItem *itm = new QTableWidgetItem(serch._db[i].name.c_str());
                       ui->tableWidget->setItem(k, j, itm);
                       j++;
                       QTableWidgetItem *itm1 = new QTableWidgetItem(serch._db[i].animal.c_str());
                       ui->tableWidget->setItem(k, j, itm1);
                       j++;
                       QTableWidgetItem *itm2 = new QTableWidgetItem(serch._db[i].disease.c_str());
                       ui->tableWidget->setItem(k, j, itm2);
                       j++;
                       QTableWidgetItem *itm3 = new QTableWidgetItem(serch._db[i].date.c_str());
                       ui->tableWidget->setItem(k, j, itm3);
                       j++;
                    }
            k++;
        }

    }
    ui->tableWidget->setRowCount(k);
}
