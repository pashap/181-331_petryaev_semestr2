#-------------------------------------------------
#
# Project created by QtCreator 2019-03-20T21:20:00
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Classes
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    reg.cpp \
    user_window.cpp \
    admin_window.cpp \
    worker.cpp \
    forget_pass.cpp \
    db.cpp

HEADERS += \
        mainwindow.h \
    reg.h \
    user_window.h \
    admin_window.h \
    worker.h \
    forget_pass.h \
    db.h

FORMS += \
        mainwindow.ui \
    reg.ui \
    user_window.ui \
    admin_window.ui \
    worker.ui \
    forget_pass.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
