﻿#ifndef USER_WINDOW_H
#define USER_WINDOW_H

#include <QDialog>

namespace Ui {
class user_window;
}

class user_window : public QDialog
{
    Q_OBJECT

public:
    explicit user_window(QWidget *parent = nullptr, QString login = "");
    ~user_window();

private slots:
    void on_exit_to_menu_clicked();

    void on_pushButton_clicked();

private:
    Ui::user_window *ui;
    QString _login;
};

#endif // USER_WINDOW_H
