#ifndef WORKER_H
#define WORKER_H

#include <QDialog>

namespace Ui {
class worker;
}

class worker : public QDialog
{
    Q_OBJECT

public:
    explicit worker(QWidget *parent = nullptr);
    ~worker();

private:
    Ui::worker *ui;
};

#endif // WORKER_H
