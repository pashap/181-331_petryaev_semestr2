#include "server.h"

#include <QTcpServer>
#include <QTcpSocket>

#include <QTextEdit>
#include <QVBoxLayout>
#include <QLabel>
#include <QDataStream>
#include <QTime>


Server::Server(quint16 port): _nextBlockSize(0){
    _tcpServer = new QTcpServer(this);

    if (!_tcpServer->listen(QHostAddress::Any, port)){
        _tcpServer->close();
        return;
    }

    connect(_tcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));

    _text = new QTextEdit();
    _text->setReadOnly(true);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(new QLabel("<H1>Server</H1>"));
    layout->addWidget(_text);
    setLayout(layout);
    resize(400,400);
}

void Server::slotNewConnection(){
    /*_clientSocket = _tcpServer->nextPendingConnection();

    connect(_clientSocket, SIGNAL(disconnected()), _clientSocket, SLOT(deleteLater()));
    connect(_clientSocket, SIGNAL(readyRead()), this, SLOT(slotReadClient()));

    sendToClient(_clientSocket, "Server response: connected.");*/
    //_clientSocket->write("Server response: connected.\n\r");

    QTcpSocket* clientSocket=_tcpServer->nextPendingConnection();
    int idusersocs=clientSocket->socketDescriptor();
    SClients[idusersocs]=clientSocket;
    connect(SClients[idusersocs],SIGNAL(readyRead()),this, SLOT(slotReadClient()));

}

void Server::slotReadClient(){
    //Вариант 1
    /*while(_clientSocket->bytesAvailable()>0){
        QByteArray array = _clientSocket->readAll();
        _clientSocket->write(array);
    }*/


    //Вариант 2
    //QTcpSocket *sckt = dynamic_cast<QTcpSocket *>(sender());
    /*if (!_clientSocket->canReadLine()) return;

    char buf[1024];
    _clientSocket->readLine(buf, sizeof(buf));
    _clientSocket->write(buf);
    //_text->setText(buf);*/

foreach(int i ,SClients.keys()){

    //Вариант 3
    QDataStream in(SClients[i]);
    //in.setVersion(QDataStream::Qt_5_10);


    while(true){
            if (_nextBlockSize == 0){
                if (SClients[i]->bytesAvailable() <static_cast<int>(sizeof(quint16))){
                    break;
                }

                in >> _nextBlockSize;
            }

            if (SClients[i]->bytesAvailable() < _nextBlockSize){
                break;
            }

            QTime time;
            QString str;
            in >> time >> str;


            if(str == "0"){
                QString message = time.toString() + " " + "server open";
                //QString message1 = time.toString() + " " + "Client has sent - " + str + ".";
                sendToClient(SClients[i], "Server response: received \"" + str + "\".");
                _nextBlockSize = 0;
                _text->append(message);
                //_text->append(message1);
                //str == "";
            }
            if(str == "1") {
                QString message = time.toString() + " " + "server close";
                sendToClient(SClients[i], "server close");
                _text->append(message);
                _tcpServer->close();
                //str == "";
            }


        _nextBlockSize = 0;

            /*QString message = time.toString() + " " + "Client has sent - " + str + ".";
            _text->append(message);



            sendToClient(_clientSocket, "Server response: received \"" + str + "\".");*/
        }
}
}

void Server::sendToClient(QTcpSocket* socket, const QString &str){
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    //out.setVersion(QDataStream::Qt_5_10);

    out << quint16(0) << QTime::currentTime() << str;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

    socket->write(arrBlock);
}


