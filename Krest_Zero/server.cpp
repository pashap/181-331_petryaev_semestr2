﻿#include "server.h"

#include <QTcpServer>
#include <QTcpSocket>

#include <QTextEdit>
#include <QVBoxLayout>
#include <QLabel>

#include <QDataStream>
#include <QTime>

Server::Server(quint16 port): _nextBlockSize(0){
    _tcpServer = new QTcpServer(this);

    if (!_tcpServer->listen(QHostAddress::Any, port)){
        _tcpServer->close();
        return;
    }

    connect(_tcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));

    _text = new QTextEdit();
    _text->setReadOnly(true);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(new QLabel("<H1>Server</H1>"));
    layout->addWidget(_text);
    setLayout(layout);
    resize(400,400);
}

void Server::slotNewConnection(){
    sockets.append(_tcpServer->nextPendingConnection());
    connect(sockets.last(),SIGNAL(readyRead()),this, SLOT(slotReadClient()));
    if (sockets.size() == 1) {
        gameState = 1;
        sendToClient(sockets.last(), "You will use X. Now we are waiting for second client.");
    }
    else if (sockets.size() == 2) {
        gameState = 2;
        sendToClient(sockets.last(), "You will use O. We are ready to play! Wait for the first move.");
        sendToClient(sockets.at(0), "Second player is in. We are ready to start! Your move is first.");

    }

}

void Server::slotReadClient(){
    for (int i = 0; i < sockets.size(); i++)
    {
        QDataStream in(sockets.at(i));
        while(true)
        {
            if (_nextBlockSize == 0)
            {
                if (sockets.at(i)->bytesAvailable() <static_cast<int>(sizeof(quint16)))
                {
                    break;
                }
                in >> _nextBlockSize;
            }

            if (sockets.at(i)->bytesAvailable() < _nextBlockSize){
                break;
            }

            QTime time;
            QString str;
            in >> time >> str;

            if (gameState == 2){
                if (i == 0){
                    int i_k = str.split(' ')[0].toInt() % 3;
                    int j_k = str.split(' ')[1].toInt() % 3;
                    if (game[i_k][j_k] != "_|") {
                        QString message = time.toString() + " " + "Client has sent - " + str + ".";
                        _text->append(message);
                        _nextBlockSize = 0;
                        sendToClient(sockets.at(0), "This cell is not empty");
                    }
                    else {
                        game[i_k][j_k] = "X";
                        if (isVictory()) {
                            game = {{"_|", "_|", "_|"}, {"_|", "_|", "_|"}, {"_|", "_|", "_|"}};
                            _nextBlockSize = 0;
                            sendToClient(sockets.at(0), "You win!!! Game end! New game is started, your turn\n\n\n\n\n\n\n\n\n\n\n\n\n\nEND GAME!!!\nEND GAME!!!\nEND GAME!!!\nEND GAME!!!");
                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "You loose. Game end! New game i started, wait for first move\n\n\n\n\n\n\n\n\n\n\n\n\n\nEND GAME!!!\nEND GAME!!!\nEND GAME!!!\nEND GAME!!!");
                            sockets.at(0)->disconnectFromHost();
                            sockets.last()->disconnectFromHost();

                        } else if (isEnd()) {
                            game = {{"_|", "_|", "_|"}, {"_|", "_|", "_|"}, {"_|", "_|", "_|"}};
                            _nextBlockSize = 0;
                            sendToClient(sockets.at(0), "Noone wins. Game end! New game is started, your turn");
                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "Noone wins. Gane end! New game i started, wait for first move");
                            sockets.at(0)->disconnectFromHost();
                        } else {
                            gameState = 3;
                            _nextBlockSize = 0;
                            sendToClient(sockets.at(0), "Great, wait for move of second player");
                            print(sockets.at(0));

                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "Now it is your turn.");
                            print(sockets.last());

                        }
                    }
                } else if (i == 1) {
                    QString message = time.toString() + " " + "Client has sent - " + str + ".";
                    _text->append(message);
                    _nextBlockSize = 0;
                    sendToClient(sockets.last(), "It is not your turn now! Wait.");
                }
            } else if (gameState == 3) {
                if (str != ""){
                    if (i == 1){
                        int i_k = str.split(' ')[0].toInt();
                        int j_k = str.split(' ')[1].toInt();
                        if (game[i_k][j_k] != "_|") {
                            QString message = time.toString() + " " + "Client has sent - " + str + ".";
                            _text->append(message);
                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "This cell is not empty");
                        }
                        else {
                            game[i_k][j_k] = "O";
                            if (isVictory()) {
                                gameState = 2;
                                game = {{"_|", "_|", "_|"}, {"_|", "_|", "_|"}, {"_|", "_|", "_|"}};
                                _nextBlockSize = 0;
                                sendToClient(sockets.last(), "You win!!! Game end! New game is started, wait for first move");
                                _nextBlockSize = 0;
                                sendToClient(sockets.at(0), "You loose. Gane end! New game i started, your turn");
                                sockets[i]->disconnectFromHost();
                            } else if (isEnd()) {
                                gameState = 2;
                                game = {{"_|", "_|", "_|"}, {"_|", "_|", "_|"}, {"_|", "_|", "_|"}};
                                _nextBlockSize = 0;
                                sendToClient(sockets.at(0), "Noone wins. Game end! New game is started, wait for first move");
                                _nextBlockSize = 0;
                                sendToClient(sockets.last(), "Noone wins. Game end! New game i started, your turn");
                            } else {
                                gameState = 2;


                                _nextBlockSize = 0;
                                sendToClient(sockets.last(), "Great, wait for move of first player");
                                print(sockets.last());
                                _nextBlockSize = 0;
                                sendToClient(sockets.at(0), "Now it is your turn.");
                                print(sockets.at(0));
                            }
                        }
                    } else if (i == 0) {
                        QString message = time.toString() + " " + "Client has sent - " + str + ".";
                        _text->append(message);
                        _nextBlockSize = 0;
                        sendToClient(sockets.at(0), "It is not your turn now! Wait.");
                    }
                }

            } else if (gameState == 1){
                QString message = time.toString() + " " + "Client has sent - " + str + ".";
                _text->append(message);
                _nextBlockSize = 0;
                sendToClient(sockets.at(0), "We are still waiting for second player.");
            }
        }

    }
}

bool Server::isVictory()
{
    for (unsigned int i = 0; i < 3; i++)
    {
        if (game[i][0] == game[i][1] && game[i][0] == game[i][2] && game[i][0] != "_|") return true;
    }
    for (unsigned int i = 0; i < 3; i++)
    {
        if (game[0][i] == game[1][i] && game[0][i] == game[2][i] && game[0][i] != "_|") return true;
    }
    if (game[0][0] == game[1][1] && game[1][1] == game[2][2] && game[0][0] != "_|") return true;
    if (game[0][2] == game[1][1] && game[1][1] == game[2][0] && game[1][1] != "_|") return true;
    return false;
}

bool Server::isEnd()
{
    if (!isVictory()){
        for (unsigned int i = 0; i < 3; i++){
            for (unsigned int j = 0; j < 3; j++){
                if (game[i][j] == "_|") return false;
            }
        }
    } else return true;
    return true;
}

void Server::print(QTcpSocket * socket)
{
    for (unsigned int i = 0; i < 3; i++)
    {
        QString line;
        line = game[i][0] + " | " + game[i][1] + " | " + game[i][2];
        _nextBlockSize = 0;
        sendToClient(socket, line);
    }
}

void Server::sendToClient(QTcpSocket* socket, const QString &str){


    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    //out.setVersion(QDataStream::Qt_5_10);

    out << quint16(0) << QTime::currentTime() << str;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

    socket->write(arrBlock);
}


