﻿#ifndef SEVER_H
#define SEVER_H

#include <QWidget>
#include <vector>

class QTcpServer;
class QTcpSocket;
class QTextEdit;

class Server: public QWidget{
    Q_OBJECT

private:
    QTcpServer *_tcpServer;
    QTcpSocket *_clientSocket;
    quint16 _nextBlockSize;
    QTextEdit *_text;
    bool state = true;
    QList<QTcpSocket*> sockets;
    void sendToClient(QTcpSocket *socket, const QString &str);

    int gameState = 0;
    std::vector<std::vector<QString>> game = {{"_|", "_|", "_|"}, {"_|", "_|", "_|"}, {"_|", "_|", "_|"}};
public:
    Server(quint16 port);
    bool get_state(){return state;}
    void change_state()
    {
        if (state) state = false;
                else state = true;
    }
    bool isVictory();
    bool isEnd();
    void print(QTcpSocket * socket);
public slots:
    virtual void slotNewConnection();
    void slotReadClient();
};

#endif // SEVER_H
